console.log('********* SERVER STARTED **********');
const fs = require('fs');
const https = require('https');
const helmet = require('helmet');

// constant vars
const port = 8000;
const options = {};
const nodemailer = require("nodemailer");
const mongoose = require('mongoose');
const validate = require('mongoose-validator');
const express = require('express');
const bodyparser = require('body-parser');
const path = require('path');
const app = express();

// if (!(process.env.DEV_MODE && process.env.DEV_MODE == 1)) {
//   options.key = fs.readFileSync("/etc/letsencrypt/live/sappercoding.com/privkey.pem");
//   options.cert = fs.readFileSync("/etc/letsencrypt/live/sappercoding.com/fullchain.pem");
// }

mongoose.Promise = global.Promise;


app.use(bodyparser.urlencoded({ extended: true }));
app.use(bodyparser.json());
app.use(express.static(__dirname + "/mortgageangular/dist"));

//connection
mongoose.connect("mongodb://localhost/PostsAPI");

// test routes connection
let Schema = mongoose.Schema;
let ContactSchema = new Schema({
  first_name: {
    type: String
  },
  last_name: {
    type: String
  },
  email: {
    type: String
  },
  phone_number: {
    type: String
  },
  company: {
    type: String
  },
  message: {
    type: String
  }
}, { timestamps: true });
Contact = mongoose.model('Contact', ContactSchema);

app.post('/contact', function (req, res) {
  console.log('****** POST CONTACT - ******', req.body);
  var contact = new Contact({ first_name: req.body.first_name, last_name: req.body.last_name, email: req.body.email, phone_number: req.body.phone_number, company: req.body.company, message: req.body.message });
  console.log('contact: ', contact);
  var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
           user: 'johnhavenbradley@gmail.com',
           pass: 'D3lw1nDrive170170'
       }
    });
    const mailOptions = {
      from: 'johnhavenbradley@gmail.com', // sender address
      to: 'johnhavenbradley@sappercoding.com, loyokelley@sappercoding.com, johnhavenbradley@gmail.com, loyokelley1986@gmail.com', // list of receivers
      subject: 'SAPPER CODING FORM SUBMISSION', // Subject line
      html: `Contact Submitted Form Info: First Name: ${contact.first_name}, Last Name: ${contact.last_name}, Email: ${contact.email}, Phone Number: ${contact.phone_number}, COMPANY: ${contact.company}, MESSAGE: ${contact.message}`// plain text body
      };
      transporter.sendMail(mailOptions, function (err, data) {
        if(err) {
          res.json({message: "Error", error: err});
        }
        else
        res.json({message: "Success", data: data});
     });
});
app.get('/contacts', function(req, res){
  console.log('***** GET CONTACT *****', req.body);
  Contact.find({}, function(err,data){
    if(err) {
      res.json({message: "Error", error: err});
    } else {
      res.json({message: "Success", data: data});
    }
  })
});


app.all('*', (req, res, next) => {
  res.sendFile(path.resolve('./mortgageangular/dist/index.html'));
});



if (!(process.env.DEV_MODE && process.env.DEV_MODE == 1)) {
  // if we're in dev mode, we skip this part...
  app.use(helmet());
  app.listen(port, function () {
    console.log(`   *PRODUCTION* Sapper Coding server.js is listening on ${port}`);
    console.log(`       DEV_MODE = ${process.env.DEV_MODE} `);
  });
  https.createServer(options, app).listen(8080);
} else {
  app.listen(port, function () {
    console.log(`   Sapper Coding server.js is listening on ${port}`);
    // console.log(`       DEV_MODE = ${process.env.DEV_MODE} `);
  });
}