import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { HomeComponent } from './pages/./home/home.component';
import { AdminGuestComponent } from './pages/./admin-guest/admin-guest.component';
import { AdminLoginComponent } from './pages/./admin-login/admin-login.component';
import { AdminOwnerComponent } from './pages/./admin-owner/admin-owner.component';
import { ApplyNowComponent } from './pages/./apply-now/apply-now.component';
import { AreasInterestComponent } from './pages/./areas-interest/areas-interest.component';
import { FindARealtorComponent } from './pages/./find-a-realtor/find-a-realtor.component';
import { LoanOptionsComponent } from './pages/./loan-options/loan-options.component';
import { OnezerothreeFormComponent } from './pages/onezerothree-form/onezerothree-form.component';
import { PurchaseAssistanceComponent } from './pages/purchase-assistance/purchase-assistance.component';
import { RealEstateComponent } from './pages/real-estate/real-estate.component';
import { RealEstateIndividualComponent } from './pages/real-estate-individual/real-estate-individual.component';
import { RecommendedProsComponent } from './pages/recommended-pros/recommended-pros.component';
import { RefinanceAdvisorComponent } from './pages/refinance-advisor/refinance-advisor.component';
import { TodaysRatesComponent } from './pages/todays-rates/todays-rates.component';
import { VaLoansComponent } from './pages/va-loans/va-loans.component';


const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'admin-guest', component: AdminGuestComponent },
  { path: 'admin-login', component: AdminLoginComponent},
  { path: 'admin-owner', component: AdminOwnerComponent},
  { path: 'apply-now', component: ApplyNowComponent},
  { path: 'areas-of-interest', component: AreasInterestComponent},
  { path: 'find-a-realtor', component: FindARealtorComponent},
  { path: 'loan-options', component: LoanOptionsComponent},
  { path: '1003-form', component: OnezerothreeFormComponent},
  { path: 'purchase-assistant', component: PurchaseAssistanceComponent},
  { path: 'real-estate', component: RealEstateComponent},
  { path: 'real-estate-individual', component: RealEstateIndividualComponent},
  { path: 'recommended-pros', component: RecommendedProsComponent},
  { path: 'refinance-advisor', component: RefinanceAdvisorComponent},
  { path: 'todays-rates', component: TodaysRatesComponent},
  { path: 'va-loans', component: VaLoansComponent},
  { path: '',
    redirectTo: '/home',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      routes,
    { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
