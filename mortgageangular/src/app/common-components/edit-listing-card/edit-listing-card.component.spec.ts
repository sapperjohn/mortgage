import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditListingCardComponent } from './edit-listing-card.component';

describe('EditListingCardComponent', () => {
  let component: EditListingCardComponent;
  let fixture: ComponentFixture<EditListingCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditListingCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditListingCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
