import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditProfessionalCardComponent } from './edit-professional-card.component';

describe('EditProfessionalCardComponent', () => {
  let component: EditProfessionalCardComponent;
  let fixture: ComponentFixture<EditProfessionalCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditProfessionalCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditProfessionalCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
