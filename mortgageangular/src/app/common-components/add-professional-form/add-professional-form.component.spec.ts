import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddProfessionalFormComponent } from './add-professional-form.component';

describe('AddProfessionalFormComponent', () => {
  let component: AddProfessionalFormComponent;
  let fixture: ComponentFixture<AddProfessionalFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddProfessionalFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddProfessionalFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
