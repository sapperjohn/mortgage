import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpService } from './http.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';

import { AddListingFormComponent } from './common-components/add-listing-form/add-listing-form.component';
import { AddProfessionalFormComponent } from './common-components/add-professional-form/add-professional-form.component';
import { ButtonListComponent } from './common-components/button-list/button-list.component';
import { DifferenceComponent } from './common-components/difference/difference.component';
import { EditListingCardComponent } from './common-components/edit-listing-card/edit-listing-card.component';
import { EditProfessionalCardComponent } from './common-components/edit-professional-card/edit-professional-card.component';
import { InfoFormComponent } from './common-components/info-form/info-form.component';
import { QuickQuoteFormComponent } from './common-components/quick-quote-form/quick-quote-form.component';
import { SpecializationsComponent } from './common-components/specializations/specializations.component';

import { HomeComponent } from './pages/home/home.component';
import { HomeBodyComponent } from './pages/home/home-body/home-body.component';
import { HomeHeaderComponent } from './pages/home/home-header/home-header.component';

import { AdminGuestComponent } from './pages/admin-guest/admin-guest.component';
import { AdminLoginComponent } from './pages/admin-login/admin-login.component';
import { AdminOwnerComponent } from './pages/admin-owner/admin-owner.component';
import { ApplyNowComponent } from './pages/apply-now/apply-now.component';
import { AreasInterestComponent } from './pages/areas-interest/areas-interest.component';
import { FindARealtorComponent } from './pages/find-a-realtor/find-a-realtor.component';
import { LoanOptionsComponent } from './pages/loan-options/loan-options.component';
import { OnezerothreeFormComponent } from './pages/onezerothree-form/onezerothree-form.component';
import { PurchaseAssistanceComponent } from './pages/purchase-assistance/purchase-assistance.component';
import { RealEstateComponent } from './pages/real-estate/real-estate.component';
import { RealEstateIndividualComponent } from './pages/real-estate-individual/real-estate-individual.component';
import { RecommendedProsComponent } from './pages/recommended-pros/recommended-pros.component';
import { RefinanceAdvisorComponent } from './pages/refinance-advisor/refinance-advisor.component';
import { TodaysRatesComponent } from './pages/todays-rates/todays-rates.component';
import { VaLoansComponent } from './pages/va-loans/va-loans.component';
import { Browser } from 'protractor';

import { FooterComponent } from './base-components/footer/footer.component';
import { NavBarComponent } from './base-components/nav-bar/nav-bar.component';
import { LoginFormComponent } from './pages/admin-login/login-form/login-form.component';
import { ApplyBodyComponent } from './pages/apply-now/apply-body/apply-body.component';
import { InterestHeaderComponent } from './pages/areas-interest/interest-header/interest-header.component';
import { InterestBodyComponent } from './pages/areas-interest/interest-body/interest-body.component';
import { RealtorBodyComponent } from './pages/find-a-realtor/realtor-body/realtor-body.component';
import { RealtorFormComponent } from './pages/find-a-realtor/realtor-form/realtor-form.component';
import { LoanBodyComponent } from './pages/loan-options/loan-body/loan-body.component';
import { QuestionsFormComponent } from './pages/loan-options/questions-form/questions-form.component';
import { PurchaseBodyComponent } from './pages/purchase-assistance/purchase-body/purchase-body.component';
import { PurchaseFormComponent } from './pages/purchase-assistance/purchase-form/purchase-form.component';
import { RealEstateListComponent } from './pages/real-estate/real-estate-list/real-estate-list.component';
import { RealEstateCardComponent } from './pages/real-estate/real-estate-card/real-estate-card.component';
import { IndividualCardComponent } from './pages/real-estate-individual/individual-card/individual-card.component';
import { RatesBodyComponent } from './pages/todays-rates/rates-body/rates-body.component';
import { FormComponent } from './pages/todays-rates/form/form.component';
import { VaBodyComponent } from './pages/va-loans/va-body/va-body.component';



@NgModule({
    declarations: [
        AppComponent,
        HomeComponent,
        AdminGuestComponent,
        AdminLoginComponent,
        AdminOwnerComponent,
        ApplyNowComponent,
        AreasInterestComponent,
        FindARealtorComponent,
        LoanOptionsComponent,
        OnezerothreeFormComponent,
        PurchaseAssistanceComponent,
        RealEstateComponent,
        RealEstateIndividualComponent,
        RecommendedProsComponent,
        RefinanceAdvisorComponent,
        TodaysRatesComponent,
        VaLoansComponent,
        NavBarComponent,
        FooterComponent,
        HomeHeaderComponent,
        HomeBodyComponent,
        AddListingFormComponent,
        AddProfessionalFormComponent,
        ButtonListComponent,
        DifferenceComponent,
        EditListingCardComponent,
        InfoFormComponent,
        QuickQuoteFormComponent,
        SpecializationsComponent,
        EditProfessionalCardComponent,
        LoginFormComponent,
        ApplyBodyComponent,
        InterestHeaderComponent,
        InterestBodyComponent,
        RealtorBodyComponent,
        RealtorFormComponent,
        LoanBodyComponent,
        QuestionsFormComponent,
        PurchaseBodyComponent,
        PurchaseFormComponent,
        RealEstateListComponent,
        RealEstateCardComponent,
        IndividualCardComponent,
        RatesBodyComponent,
        FormComponent,
        VaBodyComponent,
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        AppRoutingModule,
        FormsModule,
        BrowserAnimationsModule,
        HttpClientModule,
    ],
    providers: [
        HttpService,
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
