import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RealEstateIndividualComponent } from './real-estate-individual.component';

describe('RealEstateIndividualComponent', () => {
  let component: RealEstateIndividualComponent;
  let fixture: ComponentFixture<RealEstateIndividualComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RealEstateIndividualComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RealEstateIndividualComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
