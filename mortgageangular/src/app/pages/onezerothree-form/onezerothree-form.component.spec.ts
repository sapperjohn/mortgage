import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OnezerothreeFormComponent } from './onezerothree-form.component';

describe('OnezerothreeFormComponent', () => {
  let component: OnezerothreeFormComponent;
  let fixture: ComponentFixture<OnezerothreeFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OnezerothreeFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OnezerothreeFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
