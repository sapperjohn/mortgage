import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoanBodyComponent } from './loan-body.component';

describe('LoanBodyComponent', () => {
  let component: LoanBodyComponent;
  let fixture: ComponentFixture<LoanBodyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoanBodyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoanBodyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
