import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RatesBodyComponent } from './rates-body.component';

describe('RatesBodyComponent', () => {
  let component: RatesBodyComponent;
  let fixture: ComponentFixture<RatesBodyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RatesBodyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RatesBodyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
