import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TodaysRatesComponent } from './todays-rates.component';

describe('TodaysRatesComponent', () => {
  let component: TodaysRatesComponent;
  let fixture: ComponentFixture<TodaysRatesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TodaysRatesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TodaysRatesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
