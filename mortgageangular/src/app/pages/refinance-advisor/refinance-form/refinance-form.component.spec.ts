import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RefinanceFormComponent } from './refinance-form.component';

describe('RefinanceFormComponent', () => {
  let component: RefinanceFormComponent;
  let fixture: ComponentFixture<RefinanceFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RefinanceFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RefinanceFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
