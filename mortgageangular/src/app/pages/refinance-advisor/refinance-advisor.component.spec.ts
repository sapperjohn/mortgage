import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RefinanceAdvisorComponent } from './refinance-advisor.component';

describe('RefinanceAdvisorComponent', () => {
  let component: RefinanceAdvisorComponent;
  let fixture: ComponentFixture<RefinanceAdvisorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RefinanceAdvisorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RefinanceAdvisorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
