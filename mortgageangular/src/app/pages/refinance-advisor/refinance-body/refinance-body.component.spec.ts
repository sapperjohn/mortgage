import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RefinanceBodyComponent } from './refinance-body.component';

describe('RefinanceBodyComponent', () => {
  let component: RefinanceBodyComponent;
  let fixture: ComponentFixture<RefinanceBodyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RefinanceBodyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RefinanceBodyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
