import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplyBodyComponent } from './apply-body.component';

describe('ApplyBodyComponent', () => {
  let component: ApplyBodyComponent;
  let fixture: ComponentFixture<ApplyBodyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplyBodyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplyBodyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
