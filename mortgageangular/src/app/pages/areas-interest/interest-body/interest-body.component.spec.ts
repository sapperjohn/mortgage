import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InterestBodyComponent } from './interest-body.component';

describe('InterestBodyComponent', () => {
  let component: InterestBodyComponent;
  let fixture: ComponentFixture<InterestBodyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InterestBodyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InterestBodyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
