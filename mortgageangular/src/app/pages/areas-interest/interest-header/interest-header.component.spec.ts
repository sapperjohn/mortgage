import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InterestHeaderComponent } from './interest-header.component';

describe('InterestHeaderComponent', () => {
  let component: InterestHeaderComponent;
  let fixture: ComponentFixture<InterestHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InterestHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InterestHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
