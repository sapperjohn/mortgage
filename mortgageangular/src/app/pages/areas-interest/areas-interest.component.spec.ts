import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AreasInterestComponent } from './areas-interest.component';

describe('AreasInterestComponent', () => {
  let component: AreasInterestComponent;
  let fixture: ComponentFixture<AreasInterestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AreasInterestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AreasInterestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
