import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminGuestComponent } from './admin-guest.component';

describe('AdminGuestComponent', () => {
  let component: AdminGuestComponent;
  let fixture: ComponentFixture<AdminGuestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminGuestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminGuestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
