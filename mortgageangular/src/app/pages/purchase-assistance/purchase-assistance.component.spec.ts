import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PurchaseAssistanceComponent } from './purchase-assistance.component';

describe('PurchaseAssistanceComponent', () => {
  let component: PurchaseAssistanceComponent;
  let fixture: ComponentFixture<PurchaseAssistanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PurchaseAssistanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PurchaseAssistanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
