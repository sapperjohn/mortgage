import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PurchaseBodyComponent } from './purchase-body.component';

describe('PurchaseBodyComponent', () => {
  let component: PurchaseBodyComponent;
  let fixture: ComponentFixture<PurchaseBodyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PurchaseBodyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PurchaseBodyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
