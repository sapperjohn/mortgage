import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VaLoansComponent } from './va-loans.component';

describe('VaLoansComponent', () => {
  let component: VaLoansComponent;
  let fixture: ComponentFixture<VaLoansComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VaLoansComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VaLoansComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
