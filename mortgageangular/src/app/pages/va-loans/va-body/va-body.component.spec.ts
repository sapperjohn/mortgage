import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VaBodyComponent } from './va-body.component';

describe('VaBodyComponent', () => {
  let component: VaBodyComponent;
  let fixture: ComponentFixture<VaBodyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VaBodyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VaBodyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
