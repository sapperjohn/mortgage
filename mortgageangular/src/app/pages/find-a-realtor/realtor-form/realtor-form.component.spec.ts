import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RealtorFormComponent } from './realtor-form.component';

describe('RealtorFormComponent', () => {
  let component: RealtorFormComponent;
  let fixture: ComponentFixture<RealtorFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RealtorFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RealtorFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
