import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FindARealtorComponent } from './find-a-realtor.component';

describe('FindARealtorComponent', () => {
  let component: FindARealtorComponent;
  let fixture: ComponentFixture<FindARealtorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FindARealtorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FindARealtorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
