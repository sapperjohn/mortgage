import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RealtorBodyComponent } from './realtor-body.component';

describe('RealtorBodyComponent', () => {
  let component: RealtorBodyComponent;
  let fixture: ComponentFixture<RealtorBodyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RealtorBodyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RealtorBodyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
