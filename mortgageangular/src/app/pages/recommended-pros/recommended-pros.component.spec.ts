import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecommendedProsComponent } from './recommended-pros.component';

describe('RecommendedProsComponent', () => {
  let component: RecommendedProsComponent;
  let fixture: ComponentFixture<RecommendedProsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecommendedProsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecommendedProsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
