import { Component, OnInit} from '@angular/core';
import { HttpService } from './http.service';
declare var jquery: any;
declare var $: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  constructor(private _httpService: HttpService) { }
  ngOnInit() {
  }

  arrowClick(event) {
    console.log('BUTTON');
    window.scroll({
      top: 0,
      behavior: 'smooth'
    });

  }
 }
