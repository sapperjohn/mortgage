import { Component, OnInit } from '@angular/core';
import { HttpService } from '../../http.service';
import { ActivatedRoute, Router, RouterState, NavigationEnd } from '@angular/router';
declare var jquery: any;
declare var $: any;

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {
  linkstates = {
    'home': false,
    'todays-rates': false,
    'va-loans': false,
    'refinance-advisor': false,
    'real-estate': false,
    'loan-options': false,
    'apply-now': false,
    'find-a-realtor': false,
    'recommended-pros': false,
  };
  routerstate: any;

  constructor(
    private _service: HttpService,
    private _activatedRoute: ActivatedRoute,
    private _router: Router
  ) {
    this._router.events.forEach(
      (seg) => {
        if ( seg instanceof NavigationEnd ) {
          const activeNavSection = seg.url.toString().replace('/', '');
          console.log(`activeNavSection is ${activeNavSection}`);
          // tslint:disable:forin
          // tslint:disable-next-line:prefer-const
          for (let navlink in this.linkstates) {
            console.log(navlink);
            if ( navlink === activeNavSection ) {
              this.linkstates[navlink] = true;
            } else {
              this.linkstates[navlink] = false;
            }
          }
        }
        console.log(this.linkstates);
      }
    );
  }

  homeStyle() {
    return this.linkstates['home'];
  }

  ratesStyle() {
    return this.linkstates['todays-rates'];
  }

  vaStyle() {
    return this.linkstates['va-loans'];
  }

  refinanceStyle() {
    return this.linkstates['refinance-advisor'];
  }

  estateStyle() {
    return this.linkstates['real-estate'];
  }
  loanStyle() {
    return this.linkstates['loan-options'];
  }
  applyStyle() {
    return this.linkstates['apply-now'];
  }
  realtorStyle() {
    return this.linkstates['find-a-realtor'];
  }
  recommendedStyle() {
    return this.linkstates['recommended-pros'];
  }

  click() {
    if (this._service.show === true) {
      document.getElementById('check_box').style.display = 'block';
      this._service.show = false;
    } else {
      document.getElementById('check_box').style.removeProperty('display');
      this._service.show = true;
    }
  }

  ngOnInit() {
  }

}
